# SAURON

An Arch Linux software repository for bioinformatics / genomics / phylogenetics / population genetics. Any Arch Linux system with `pacman` can use this repository.

## Disclaimer

I am maintaining this repository with software and versions of software that I use. I make no claims regarding functionality, usability, etc. In fact, if you choose to use this repository, you do so completely accepting that I will not be held responsible for anything whatsoever that goes wrong with your system.

## Installation

Making this repository available to your Arch Linux (or derivative) system is quite easy. Edit your ```/etc/pacman.conf``` file and paste the following lines at the bottom:

```
[sauron]
SigLevel = Optional DatabaseOptional
Server = https://gitlab.com/albevdmerwe/sauron/-/raw/main/x86_64
```
Then simply do a repository update with ```pacman```:

```
[...]$ sudo pacman -Syu
```

You will see output similar to this:

```
:: Synchronizing package databases...
 core is up to date
 extra is up to date
 community is up to date
 multilib is up to date
 sauron                                   482.0   B  1147   B/s 00:00 [#######################################] 100%
:: Starting full system upgrade...
 there is nothing to do
```

The fact that ```sauron``` is indicated after ```multilib``` means that the new repository is now available on your system.

## Package installation

Search for one of the packages, let's say ```multilocus```:

```
[...]$ pacman -Ss multilocus
sauron/multilocus 1.6-0
    Analysing multilocus population data.
```

Notice ```sauron/multilocus```. You can install this package using ```sudo pacman -S sauron/multilocus```. Then you can verify that the package is indeed installed:

```
[...]$ which multilocus
/usr/bin/multilocus
```

## Package removal

To remove a package that was installed from the Sauron repository, do the following:

```
[...]$ sudo pacman -Rns sauron/multilocus
```

## (optional) Manual package building

Install the ```base-devel``` metapackage before you attempt to build any packages:

```
[...]$ sudo pacman -S --needed base-devel
```

If you feel that you do not trust the prebuilt packages in the repository, please have a look at the ```buildfiles``` directory. You can easily download a ```PKGBUILD``` file to a clean directory on your computer. Inspect the contents of the ```PKGBUILD``` file to make sure that you agree with the contents. Once you're OK with it, build a package. In the directory containing the PKGBUILD file,

```
[...]$ makepkg
```

This will build a file ending with a ```zst``` extension - that is the binary package. Install this package on your system with

```
[...]$ sudo pacman -U *.zst
```

## Packages


|  Package   | Version   | Description                                                                        |
|------------|-----------|------------------------------------------------------------------------------------|
| bamtools   | 2.5.2     | A programmer's API and an end-user's toolkit for handling BAM files.               |
| bcftools   | 1.13      | utilities for post-processing alignments in the SAM, BAM, and CRAM formats.        |
| blast+-bin | 2.12.0    | New suite of BLAST tools that utilizes the NCBI C++ Toolkit                        |
| htslib     | 1.13      | A unified C library for accessing common file formats, such as SAM, CRAM and VCF   |
| multilocus | 1.6       | Analysing multilocus population data.                                              |
| samtools   | 1.13      | Tools (written in C using htslib) for manipulating next-generation sequencing data.|